/* hw5.c */
/* 複数の都市を巡って戻ったときの距離が短いルートを探そう */
/* ４分割して考える */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>  //sqrt用

#define CITIES  512
#define  FILENAME "input_5.csv"

int Plus(int x){  //計算で使う
  int ret=0;
  while(x>0){
    ret += x;
    x--;
  }
  return ret;
}

void Swap(float *x, float *y){
    int  work;

    work = *x;        /* xの値を一時保存 */
    *x = *y;          /* xの値をyの値に書き換える */
    *y = work;        /* yの値をxの値に書き換える */
}

float km(float x1, float y1, float x2, float y2){  //２点間の距離を求める
  float ret = 0.0;
  ret = (float)sqrt((double)((x1-x2)*(x1-x2) + (y1-y2)* (y1-y2)));
  return ret;
}

int main(){

  FILE *fp;
  char tmp[256];
  char *mark1,*mark2;
  float x[CITIES],y[CITIES];
  float x_max=0.0, y_max=0.0;
  int i=0;

  if((fp = fopen(FILENAME,"r"))==NULL){
    printf("ファイルが上手く読み込めません\n");
    return -1;
  }

    while(fgets(tmp,256,fp)){
      sscanf(tmp,"%f,%f",&x[i],&y[i]);
      if(x[i]>x_max)   x_max=x[i];
      if(y[i]>y_max)   y_max=y[i];
      i++;
    } 

    //printf("x_max = %f, y_max = %f\n",x_max,y_max); //チェック用
    /* 
    for(i=1;i<CITIES+1;i++){  //チェック用
    printf("x[%d]=%f, y[%d]=%f\n",i,x[i],i,y[i]);
  }
    */
    float x1[CITIES],y1[CITIES];
    float x2[CITIES],y2[CITIES];
    float x3[CITIES],y3[CITIES];
    float x4[CITIES],y4[CITIES];
    int N1=0,N2=0,N3=0,N4=0;

    for(i=1;i<CITIES+1;i++){ 
      if(x[i]>x_max/2 && y[i]>y_max/2){
	Swap(&x1[N1+1],&x[i]);
	Swap(&y1[N1+1],&y[i]);
	N1++;
      }
      else if(x[i]<=x_max/2 && y[i]>=y_max/2){
	Swap(&x2[N2+1],&x[i]);
	Swap(&y2[N2+1],&y[i]);
	N2++;
      }
      else if(x[i]<x_max/2 && y[i]<y_max/2){
	Swap(&x3[N3+1],&x[i]);
	Swap(&y3[N3+1],&y[i]);
	N3++;
      }
      else{
	Swap(&x4[N4+1],&x[i]);
	Swap(&y4[N4+1],&y[i]);
	N4++;
      }
    }

    unsigned int NUMKYORI1 = 0,NUMKYORI2 = 0,NUMKYORI3 = 0,NUMKYORI4 = 0;
    NUMKYORI1 = (N1 * (N1-1)) / 2 ;
    NUMKYORI2 = (N2 * (N2-1)) / 2 ;
    NUMKYORI3 = (N3 * (N3-1)) / 2 ;
    NUMKYORI4 = (N4 * (N4-1)) / 2 ;
    
    float kyori1[NUMKYORI1+1];
    float kyori2[NUMKYORI2+1];
    float kyori3[NUMKYORI3+1];
    float kyori4[NUMKYORI4+1];
    int j;
    float ans1 = 0.0,ans2 = 0.0,ans3 = 0.0,ans4 = 0.0;
    /*
    printf("N1 = %d\n",N1);  //チェック用
    printf("NUMKYORI1 %u\n",NUMKYORI1);  //チェック用

    for(i=1;i<N1+1;i++){  //チェック用
      printf("x1[%d]=%f, y1[%d]=%f\n",i,x1[i],i,y1[i]);
	}
    for(i=1;i<N2+1;i++){  //チェック用
      printf("x2[%d]=%f, y2[%d]=%f\n",i,x2[i],i,y2[i]);
	}
       
    printf("N1 = %d\n",N1);  //チェック用
    printf("N2 = %d\n",N2);  //チェック用
    printf("N3 = %d\n",N3);  //チェック用
    printf("N4 = %d\n",N4);  //チェック用
    */

    if(N1 != 1){
      for(i=0;i<N1-1;i++){
	for(j=1;j<N1-i;j++){
	  kyori1[j+N1 * i - Plus(i)] = km(x1[j],y1[j],x1[j+1+i],y1[j+1+i]);
	}
      }
    }
    
    if(N2 != 1){
      for(i=0;i<N2-1;i++){
	for(j=1;j<N2-i;j++){
	  kyori2[j+N2 * i - Plus(i)] = km(x2[j],y2[j],x2[j+1+i],y2[j+1+i]);
	}
      }
    }

    if(N3 != 1){
      for(i=0;i<N3-1;i++){
	for(j=1;j<N3-i;j++){
	  kyori3[j+N3 * i - Plus(i)] = km(x3[j],y3[j],x3[j+1+i],y3[j+1+i]);
	}
      }
    }

    if(N4 != 1){
      for(i=0;i<N4-1;i++){
	for(j=1;j<N4-i;j++){
	  kyori4[j+N4 * i - Plus(i)] = km(x4[j],y4[j],x4[j+1+i],y4[j+1+i]);
	}
      }
    }


    /*
    for(j=1;j<NUMKYORI1+1;j++){  //チェック用
      printf("kyori1[%d]=%fである。\n",j,kyori1[j]);
    }
    for(j=1;j<NUMKYORI2+1;j++){
      printf("kyori2[%d]=%fである。\n",j,kyori2[j]);
    }
    for(j=1;j<NUMKYORI3+1;j++){
      printf("kyori3[%d]=%fである。\n",j,kyori3[j]);
    }
    for(j=1;j<NUMKYORI4+1;j++){
      printf("kyori4[%d]=%fである。\n",j,kyori4[j]);
    }
    */
    for(j=1;j<N1;j++){
      ans1 += kyori1[j];
    }
    for(j=1;j<N2;j++){
      ans2 += kyori2[j];
    }
    for(j=1;j<N3;j++){
      ans3 += kyori3[j];
    }
    for(j=1;j<N4;j++){
      ans4 += kyori4[j];
    }

    float tie1=0.0,tie2=0.0,tie3=0.0,tie4=0.0;

    //4つに分割したグループ間をつなぎたい
    if(N1!=0 || N2 !=0){
    tie1 = km(x1[1],y1[1],x2[N2],y2[N2]);
    }
    if(N2!=0 || N3 !=0){
    tie2 = km(x2[1],y2[1],x3[N3],y3[N3]);
    }
    if(N3!=0 || N4 !=0){
    tie3 = km(x3[1],y3[1],x4[N4],y4[N4]);
    }
    if(N4!=0 || N1 !=0){
    tie4 = km(x4[1],y4[1],x1[N1],y1[N1]);
    }
    /*
    printf("ans1距離は、%f\n",ans1);  //チェック用
    printf("ans2距離は、%f\n",ans2);
    printf("ans3距離は、%f\n",ans3);
    printf("ans4距離は、%f\n",ans4);
    */ 
    float final_ans=0.0;
    final_ans = ans1 + ans2 + ans3 + ans4 + tie1 + tie2 + tie3 + tie4;
    printf("距離は、%f\n",final_ans);
}


Yuiho Ishida
